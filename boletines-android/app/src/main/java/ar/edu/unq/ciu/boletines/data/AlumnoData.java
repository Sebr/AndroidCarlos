package ar.edu.unq.ciu.boletines.data;

/**
 * Created by muerte on 12/12/17.
 */

public class AlumnoData {
    private String dni;
    private String nombreYApellido;
    private String curso;
    private String mail;

    // constructor

    AlumnoData(String dni, String nombreYApellido) {
        super();
        this.dni = dni;
        this.nombreYApellido = nombreYApellido;

    }

    // getters / setters

    void setCurso(String curso) {
        this.curso = curso;
    }

    public String getDni() {
        return this.dni;
    }

    public String getNombreYApellido() {
        return this.nombreYApellido;
    }

    public String getCurso() {
        return this.curso;
    }


    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
