package ar.edu.unq.ciu.boletines.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muerte on 12/12/17.
 */

class MateriaDataAnalizer {

    static List<MateriaData> buildListaDeMaterias(JSONArray jsonData) {
        List<MateriaData> mds = new ArrayList<>();
        try {
            for (int i = 0; i < jsonData.length(); i++) {
                JSONObject jsa = jsonData.getJSONObject(i);
                MateriaData ad = new MateriaData(jsa.getString("nombre"),
                                        MateriaDataAnalizer.getDato(jsa, "nota1"),
                                        MateriaDataAnalizer.getDato(jsa, "nota2"),
                                        MateriaDataAnalizer.getDato(jsa, "nota3"),
                                        MateriaDataAnalizer.getDato(jsa, "condicion"));
                mds.add(ad);
            }
        } catch (JSONException e) {
            // puede que al curso aún no se le hayan agregado materias
            // puede que el alumno no tenga registradas notas en algunos trimestres
            // => no pasa nada
        }
        return mds;
    }

    static private String getDato(JSONObject jsa, String key) {
        try {
            return jsa.getString(key);
        } catch (JSONException e) {
            return "-";
        }
    }

}
