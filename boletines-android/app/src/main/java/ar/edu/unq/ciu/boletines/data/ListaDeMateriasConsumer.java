package ar.edu.unq.ciu.boletines.data;

import com.android.volley.VolleyError;

import java.util.List;

/**
 * Created by muerte on 12/12/17.
 */

public interface ListaDeMateriasConsumer {

    void accept(List<MateriaData> data);

    void handleError(int stringId, VolleyError error, String url);

}
