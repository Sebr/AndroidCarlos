package ar.edu.unq.ciu.boletines.data;

/**
 * Created by muerte on 12/12/17.
 */

public class MateriaData {

    private final String nombre;
    private final String nota1;
    private final String nota2;
    private final String nota3;
    private final String condicion;

    MateriaData(String nombre, String nota1, String nota2, String nota3, String condicion) {
        this.nombre = nombre;
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.nota3 = nota3;
        this.condicion = condicion;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getNota1() {
        return this.nota1;
    }

    public String getNota2() {
        return this.nota2;
    }

    public String getNota3() {
        return this.nota3;
    }

    public String getCondicion() {
        return this.condicion;
    }

}
