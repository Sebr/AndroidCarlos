package ar.edu.unq.ciu.boletines.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import ar.edu.unq.ciu.boletines.tools.ApplicationToolset;
import ar.edu.unq.ciu.boletines.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ApplicationToolset.setContext(this);

        findViewById(R.id.btn_alumnos).setOnClickListener(view -> this.mostrarAlumnos());
        findViewById(R.id.btn_boletines).setOnClickListener(view -> this.mostrarBoletines());
        findViewById(R.id.btn_cursos).setOnClickListener(view -> this.mostrarCursos());
        findViewById(R.id.btn_notas).setOnClickListener(view -> this.mostrarNotas());
    }

    private void mostrarAlumnos() {
        // TODO
       // Toast.makeText(this, "// TODO: ver alumnos", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ListaDeAlumnosCris.class);
        startActivity(intent);
    }

    private void mostrarBoletines() {
        Intent intent = new Intent(this, ListaDeAlumnosActivity.class);
        startActivity(intent);
    }

    private void mostrarCursos() {
        // TODO
        Toast.makeText(this, "// TODO: ver cursos", Toast.LENGTH_SHORT).show();
    }

    private void mostrarNotas() {
        // TODO
        Toast.makeText(this, "// TODO: ver notas", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.mi_opciones) {
            Intent intent = new Intent(this, OpcionesActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
