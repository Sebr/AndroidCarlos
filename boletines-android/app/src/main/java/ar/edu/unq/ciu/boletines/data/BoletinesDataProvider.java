package ar.edu.unq.ciu.boletines.data;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;

import ar.edu.unq.ciu.boletines.tools.ApplicationToolset;
import ar.edu.unq.ciu.boletines.tools.Opciones;
import ar.edu.unq.ciu.boletines.R;

/**
 * Created by muerte on 12/12/17.
 */

public class BoletinesDataProvider {

    static public void fetchDNIsYNombresDeAlumnos(ListaDeAlumnosConsumer consumer) {
        String url = Opciones.getUrl() + "/alumnos";

        JsonArrayRequest jsar = new JsonArrayRequest(Request.Method.GET, url, null,
            response -> consumer.accept(AlumnoDataAnalizer.buildListaDeAlumnos(consumer, response)),
            error -> {
                consumer.handleError(R.string.error_req_rest, error, url);
//                error.printStackTrace();
//                throw new RuntimeException("error en el request REST", error);
            }
        );

        ApplicationToolset.toolset().addToRequestQueue(jsar);
    }

    static public void fetchMateriasDelAlumno(String dni, ListaDeMateriasConsumer consumer) {
        String url = Opciones.getUrl() + "/alumno/" + dni;

        JsonArrayRequest jsor = new JsonArrayRequest(Request.Method.GET, url, null,
            response -> consumer.accept(MateriaDataAnalizer.buildListaDeMaterias(response)),
            error -> {
                consumer.handleError(R.string.error_req_rest, error, url);
//                error.printStackTrace();
//                throw new RuntimeException(activity.getString(R.string.error_req_rest), error);
            }
        );

        ApplicationToolset.toolset().addToRequestQueue(jsor);
    }

}
