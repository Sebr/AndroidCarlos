package ar.edu.unq.ciu.boletines.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ar.edu.unq.ciu.boletines.R;

import ar.edu.unq.ciu.boletines.data.AlumnoData;


/**
 * Created by cris on 13/3/2018.
 */

public class ListaAlumnosCrisAdapter extends ArrayAdapter {
    private LayoutInflater inflater;
    public ListaAlumnosCrisAdapter(@NonNull Context context,  @NonNull List objects) {
        super(context, 0, objects);
        this.inflater = LayoutInflater.from(context);
    }
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = this.inflater.inflate(R.layout.alumnos_adapter_cris, parent, false);
        }

        AlumnoData alumno = (AlumnoData) this.getItem(position);

        if (alumno != null) {
            TextView tvDni = convertView.findViewById(R.id.tv_dni);
            tvDni.setText(alumno.getDni());

            TextView tvNombreYApellido = convertView.findViewById(R.id.tv_nombre_y_apellido);
            tvNombreYApellido.setText(alumno.getNombreYApellido());

            TextView tvCurso = convertView.findViewById(R.id.tv_curso);
            tvCurso.setText(alumno.getCurso());

            TextView tvMail = convertView.findViewById(R.id.tv_email);
            tvMail.setText(alumno.getMail());
        }

        return convertView;
    }
        }

