package ar.edu.unq.ciu.boletines.tools;

/**
 * Created by muerte on 12/12/17.
 */

public class Opciones {

    private static String ip = "192.168.0.103";//"calm-crag-44048.herokuapp.com"; // local: 192.168.0.101, etc.1
    private static String port = "8080";    // local: 8080
    private static String base = "boletines";    // local: /boletines

    public static String getIp() {
        return Opciones.ip;
    }

    public static String getPort() {
        return Opciones.port;
    }

    public static String getBase() {
        return Opciones.base;
    }

    public static void setIp(String ip) {
        Opciones.ip = ip;
    }

    public static void setPort(String port) {
        Opciones.port = port;
    }

    public static void setBase(String base) {
        Opciones.base = base;
    }

    public static String getUrl() {
        return "http://" + Opciones.ip + Opciones.getPortConFormato() + Opciones.getBaseConFormato() + "/api";
    }

    private static String getPortConFormato() {
        if (port.isEmpty()) {
            return "";
        } else {
            return ":" + Opciones.port;
        }
    }

    public static String getBaseConFormato() {
        if (base.isEmpty()) {
            return "";
        } else {
            return "/" + Opciones.base;
        }
    }
}
