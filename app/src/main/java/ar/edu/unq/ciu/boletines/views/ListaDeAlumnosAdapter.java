package ar.edu.unq.ciu.boletines.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ar.edu.unq.ciu.boletines.R;
import ar.edu.unq.ciu.boletines.data.AlumnoData;

/**
 * Created by muerte on 12/12/17.
 */

public class ListaDeAlumnosAdapter extends ArrayAdapter<AlumnoData> {

    private LayoutInflater inflater;

    ListaDeAlumnosAdapter(@NonNull Context context, List<AlumnoData> objects) {
        super(context, 0, objects);
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = this.inflater.inflate(R.layout.alumno_list_item, parent, false);
        }

        AlumnoData alumno = getItem(position);

        if (alumno != null) {
            TextView tvDni = convertView.findViewById(R.id.tv_dni);
            tvDni.setText(alumno.getDni());

            TextView tvNombreYApellido = convertView.findViewById(R.id.tv_nombre_y_apellido);
            tvNombreYApellido.setText(alumno.getNombreYApellido());
        }

        return convertView;
    }

}
