package ar.edu.unq.ciu.boletines.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;

import java.util.List;

import ar.edu.unq.ciu.boletines.R;
import ar.edu.unq.ciu.boletines.data.BoletinesDataProvider;
import ar.edu.unq.ciu.boletines.data.CursoData;
import ar.edu.unq.ciu.boletines.data.CursosDataProvider;
import ar.edu.unq.ciu.boletines.data.ListaDeCursosConsumer;
import ar.edu.unq.ciu.boletines.data.ListaDeMateriasConsumer;
import ar.edu.unq.ciu.boletines.data.MateriaData;


public class CursoActivity extends AppCompatActivity {

    private ListView lvCursos;

    private ListaCursosAdapter adapter;
    private TextView tvListaVacia;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_cursos);
        setTitle("Cursos");

        Intent intent = getIntent();
        String anio = intent.getStringExtra("anio");
        String division = intent.getStringExtra("division");
        String yurno = intent.getStringExtra("turno");

        TextView tvAnio = findViewById(R.id.tv_anio);
        tvAnio.setText(anio);

        this.tvListaVacia = findViewById(R.id.tv_lista_vacia);

        if (anio == null) {
            this.setEmptyMsg("");
            this.hideProgressBar();
        } else {
            TextView tvCurso = findViewById(R.id.tv_curso);
            tvCurso.setText(anio);

            this.lvCursos = findViewById(R.id.lv_materias);
            this.lvCursos.setEmptyView(this.tvListaVacia);

            CursosDataProvider.fetchCursos(new ListaDeCursosConsumer() {
//
                @Override
                public void accept(List<CursoData> data) {
                    if (data.isEmpty()) {
                        CursoActivity.this.setEmptyMsg(getString(R.string.no_datos_materias));
                    } else {
                        CursoActivity.this.adapter = new ListaCursosAdapter(CursoActivity.this, data);
                        CursoActivity.this.lvCursos.setAdapter(CursoActivity.this.adapter);
                    }
                    CursoActivity.this.hideProgressBar();
                }

                @Override
                public void handleError(int stringId, VolleyError error, String url) {
                    Log.e(getString(R.string.cursos), getString(stringId), error);
                    Log.d(getString(R.string.cursos), "url: " + url);
                    CursoActivity.this.setEmptyMsg(getString(stringId));
                    CursoActivity.this.hideProgressBar();
                }

                @Override
                public void handleException(int error_parsing_json, JSONException e) {

                }
            });
        }
    }

    public void hideProgressBar() {
        findViewById(R.id.pb_cargando).setVisibility(View.GONE);
    }

    public void setEmptyMsg(String msg) {
        this.tvListaVacia.setText(msg);
    }

}
