package ar.edu.unq.ciu.boletines.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ar.edu.unq.ciu.boletines.data.BoletinesDataProvider;
import ar.edu.unq.ciu.boletines.R;

public class Notas extends AppCompatActivity  {


    private List<Integer> idsCurso = new ArrayList<>();
    private List<String> listaConNombresDeCursos = new ArrayList<>();
    private Spinner spinnerCurso;
    private Spinner spinnerMatria;
    private Spinner spinnerTrimestre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notas);

        spinnerCurso = findViewById(R.id.spinner);
        spinnerMatria = findViewById(R.id.spinner2);
        spinnerTrimestre = findViewById(R.id.spinner3);

        ArrayList<String> paraPrimerSpinner = new ArrayList<>();



        //Quedan hardcodeados
        String [] col2 = {"Matematica", "Fisica", "Quimica","Informatica"}; //Materia
        String [] col3 = {"1", "2", "3"};//Trimestres

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,col2);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,col3);

        spinnerTrimestre.setAdapter(adapter3);
        spinnerMatria.setAdapter(adapter2);

        this.popularSpinnerConCursos();
    }


    public void  botonApretado(View view){
        this.siguienteActivity();
    }

    public List<Integer> getIdsCurso(){
        return idsCurso;
    }

    public List<String> getListaConNombresDeCursos() {
        return listaConNombresDeCursos;
    }

    public void popularSpinnerConCursos (){

        RequestQueue rq;
        rq = Volley.newRequestQueue(this);
        JsonArrayRequest recuest = new JsonArrayRequest(
                Request.Method.GET,
                "http://192.168.1.13:8080/boletines/api2/cursos",
                null,
                datos-> procesar(datos),
                err-> Log.v("pedro",err.toString())
        );

        rq.add(recuest);
    }

    private void procesar(JSONArray data) {
        for (int i = 0; i < data.length() ; i++) {
            try {
                JSONObject datum = data.getJSONObject(i);

                this.listaConNombresDeCursos.add(datum.getString("anio")+ " " + datum.getString("turno")+ " " + datum.getString("division"));
                idsCurso.add(datum.getInt("id"));

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,this.listaConNombresDeCursos);
        spinnerCurso.setAdapter(adapter1);
    }


    public String armarUrl(){

        return "http://192.168.1.13:8080/boletines/api2/cursoEspecifico/" +
                ( this.getIdsCurso().get(spinnerCurso.getSelectedItemPosition()) ) +   "/materia/" + spinnerMatria.getSelectedItem().toString() + "/trimestre/" +
                   spinnerTrimestre.getSelectedItem().toString();
    }


    public void siguienteActivity(){
        Intent intent = new Intent(Notas.this,MostrarNotasAlumnos.class);
        String url = this.armarUrl();
        intent.putExtra("UrlConsulta",url);
        startActivity(intent);
    }


}

