package ar.edu.unq.ciu.boletines.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.android.volley.VolleyError;

import org.json.JSONException;

import java.util.List;

import ar.edu.unq.ciu.boletines.R;
import ar.edu.unq.ciu.boletines.data.AlumnoData;
import ar.edu.unq.ciu.boletines.data.BoletinesDataProvider;
import ar.edu.unq.ciu.boletines.data.CursoData;
import ar.edu.unq.ciu.boletines.data.CursosDataProvider;
import ar.edu.unq.ciu.boletines.data.ListaDeAlumnosConsumer;
import ar.edu.unq.ciu.boletines.data.ListaDeCursosConsumer;

public class ListaDeCursos extends AppCompatActivity {
private ListView listView;
private ListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_cursos);
        listView = findViewById(R.id.listview);
        setTitle("Cursos");

        CursosDataProvider.fetchCursos(new ListaDeCursosConsumer() {
            @Override
            public void accept(List<CursoData> data) {
                ListaDeCursos.this.adapter = new ListaCursosAdapter(ListaDeCursos.this, data);
                ListaDeCursos.this.listView.setAdapter(ListaDeCursos.this.adapter);
//                ListaDeCursos.this.hideProgressBar();
            }

            @Override
            public void handleError(int stringId, VolleyError error, String url) {
                Log.e(getString(R.string.cursos), getString(stringId), error);
                Log.d(getString(R.string.cursos), "url: " + url);
//                ListaDeCursos.this.setEmptyMsg(getString(stringId) + "\n\n" + error.getMessage());
//                ListaDeCursos.this.hideProgressBar();
            }

            @Override
            public void handleException(int stringId, JSONException exception) {
                Log.e(getString(R.string.cursos), getString(stringId), exception);
//                ListaDeCursos.this.setEmptyMsg(getString(stringId));
//                ListaDeCursos.this.hideProgressBar();
            }
        });
        this.listView.setOnItemClickListener((parent, view, position, id) -> {
            CursoData cursoSeleccionado = (CursoData) ListaDeCursos.this.adapter.getItem(position);

            if (cursoSeleccionado != null) {
                Intent intent = new Intent(ListaDeCursos.this, CursoActivity.class);
                intent.putExtra("anio", cursoSeleccionado.getAnio());
                intent.putExtra("division", cursoSeleccionado.getDivision());
                intent.putExtra("turno", cursoSeleccionado.getTurno());
                startActivity(intent);
            }
        });
    }
    }




