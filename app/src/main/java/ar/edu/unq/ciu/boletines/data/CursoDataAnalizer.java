package ar.edu.unq.ciu.boletines.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ar.edu.unq.ciu.boletines.R;

class CursoDataAnalizer {

    static List<CursoData> buildListaDeCursos(ListaDeCursosConsumer consumer, JSONArray jsonData) {
        List<CursoData> res = new ArrayList<>();
        try {
            for (int i = 0; i < jsonData.length(); i++) {
                JSONObject jsa = jsonData.getJSONObject(i);
                res.add(CursoDataAnalizer.decodeCurso(consumer, jsa));
            }
        } catch (JSONException e) {
            consumer.handleException(R.string.error_parsing_json, e);
//            throw new RuntimeException(activity.getString(R.string.error_parsing_json), e);
        }
        return res;
    }

    static private String getDato(JSONObject jsa, String key) {
        try {
            return jsa.getString(key);
        } catch (JSONException e) {
            return "-";
        }
    }

    static private CursoData decodeCurso(ListaDeCursosConsumer consumer, JSONObject jsa) {
        CursoData ad = null;
        try {
            ad = new  CursoData( jsa.getString("anio"), jsa.getString("division"), jsa.getString("turno"));

        } catch (JSONException e) {
            consumer.handleException(R.string.error_parsing_json, e);
//            throw new RuntimeException(activity.getString(R.string.error_parsing_json), e);
        }
        return ad;
    }

}
