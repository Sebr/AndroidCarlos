package ar.edu.unq.ciu.boletines.data;


public class CursoData {
        private String anio;
        private String division;
        private String turno;


    CursoData(String anio, String division, String turno) {
        this.anio = anio;
        this.division = division;
        this.turno = turno;
        }

        public String getAnio() {return anio;}

        public String getDivision() {return division;}

        public String getTurno() {return turno;}


}


