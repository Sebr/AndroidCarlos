package ar.edu.unq.ciu.boletines.data;

public class AlumnoDataConNota {

    private int nota;
    private String nombreYApellido;
    private String trimestre;

    // constructor

    AlumnoDataConNota(String trimestre, String nombreYApellido, int nota) {
        super();
        this.trimestre = trimestre;
        this.nota = nota;
        this.nombreYApellido = nombreYApellido;

    }


    public int getNota() {
        return this.nota;
    }

    public String getNombreYApellido() {
        return this.nombreYApellido;
    }

    public String getTrimestre() {
        return this.trimestre;
    }
}
