package ar.edu.unq.ciu.boletines.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;

import java.util.List;

import ar.edu.unq.ciu.boletines.R;
import ar.edu.unq.ciu.boletines.data.BoletinesDataProvider;
import ar.edu.unq.ciu.boletines.data.ListaDeMateriasConsumer;
import ar.edu.unq.ciu.boletines.data.MateriaData;

/**
 * Created by muerte on 12/12/17.
 */

public class BoletinActivity extends AppCompatActivity {

    private ListView lvMaterias;

    private ListaDeMateriasAdapter adapter;
    private TextView tvListaVacia;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boletin);
        setTitle(getString(R.string.boletin));

        Intent intent = getIntent();
        String dni = intent.getStringExtra("dni");
        String nombreYApellido = intent.getStringExtra("nombre_y_apellido");
        String curso = intent.getStringExtra("curso");

        TextView tvAlumno = findViewById(R.id.tv_alumno);
        tvAlumno.setText(nombreYApellido);

        this.tvListaVacia = findViewById(R.id.tv_lista_vacia);

        if (curso == null) {
            this.setEmptyMsg(getString(R.string.alumno_no_inscripto));
            this.hideProgressBar();
        } else {
            TextView tvCurso = findViewById(R.id.tv_curso);
            tvCurso.setText(curso);

            this.lvMaterias = findViewById(R.id.lv_materias);
            this.lvMaterias.setEmptyView(this.tvListaVacia);

            BoletinesDataProvider.fetchMateriasDelAlumno(dni, new ListaDeMateriasConsumer() {
                @Override
                public void accept(List<MateriaData> data) {
                    if (data.isEmpty()) {
                        BoletinActivity.this.setEmptyMsg(getString(R.string.no_datos_materias));
                    } else {
                        BoletinActivity.this.adapter = new ListaDeMateriasAdapter(BoletinActivity.this, data);
                        BoletinActivity.this.lvMaterias.setAdapter(BoletinActivity.this.adapter);
                    }
                    BoletinActivity.this.hideProgressBar();
                }

                @Override
                public void handleError(int stringId, VolleyError error, String url) {
                    Log.e(getString(R.string.app_name), getString(stringId), error);
                    Log.d(getString(R.string.app_name), "url: " + url);
                    BoletinActivity.this.setEmptyMsg(getString(stringId));
                    BoletinActivity.this.hideProgressBar();
                }
            });
        }
    }

    public void hideProgressBar() {
        findViewById(R.id.pb_cargando).setVisibility(View.GONE);
    }

    public void setEmptyMsg(String msg) {
        this.tvListaVacia.setText(msg);
    }

}
