package ar.edu.unq.ciu.boletines.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ar.edu.unq.ciu.boletines.R;

public class MostrarNotasAlumnos extends AppCompatActivity {

    private ListView listAlumnos;
    private SimpleAdapter alumnosAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_notas_alumnos);

        listAlumnos = (ListView) findViewById(R.id.list);

        this.popularListViewAlumnosConNotas(this.getIntent().getExtras().getString("UrlConsulta"));

    }

    public void popularListViewAlumnosConNotas (String url){

        Log.v("loquetienequemostrar",url);
        RequestQueue rq;
        rq = Volley.newRequestQueue(this);
        JsonArrayRequest recuest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                datos-> procesar3ConSimpleList(datos),
                err-> Log.v("pedro",err.toString())
        );

        rq.add(recuest);
    }

    private void procesar2(JSONArray data) {
        List<String> col1 = new ArrayList<>();

        for (int i = 0; i < data.length() ; i++) {
            try {
                JSONObject datum = data.getJSONObject(i);

                col1.add("Materia:" + datum.get("materia")+ "," + " " +  "Nota:" + datum.getString("nota")+ "," + " " + "Nombre y Apellido:" + datum.get("nombreYApellido") + " " + "Trimestre:" + datum.get("trimestre"));


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,col1);
        listAlumnos.setAdapter(adapter);
    }



    private void procesar3ConSimpleList(JSONArray data) {

        List<Map<String, String>> alumnosConNotas = new ArrayList<>();

        for (int i = 0; i < data.length() ; i++) {
            try {
                JSONObject datum = data.getJSONObject(i);

                Map<String, String> alumnoConNota = new HashMap<>();

                alumnoConNota.put("nombreYApellido",datum.get("nombreYApellido").toString());
                alumnoConNota.put("nota",datum.getString("nota"));
                alumnosConNotas.add(alumnoConNota);

                //Asi estaba antes choclo de strings...
               // col1.add("Materia:" + datum.get("materia")+ "," + " " +  "Nota:" + datum.getString("nota")+ "," + " " + "Nombre y Apellido:" + datum.get("nombreYApellido") + " " + "Trimestre:" + datum.get("trimestre"));


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        this.alumnosAdapter = new SimpleAdapter(this,alumnosConNotas, android.R.layout.simple_expandable_list_item_2,new String[]{"nombreYApellido","nota"},new int[] {android.R.id.text1, android.R.id.text2 });
        listAlumnos.setAdapter(alumnosAdapter);
    }

}
