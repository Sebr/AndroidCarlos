package ar.edu.unq.ciu.boletines.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;

import java.util.List;

import ar.edu.unq.ciu.boletines.R;
import ar.edu.unq.ciu.boletines.data.AlumnoData;
import ar.edu.unq.ciu.boletines.data.BoletinesDataProvider;
import ar.edu.unq.ciu.boletines.data.ListaDeAlumnosConsumer;

/**
 * Created by muerte on 12/12/17.
 */

public class ListaDeAlumnosActivity extends AppCompatActivity {

    private ListView lvAlumnos;
    private ListaDeAlumnosAdapter adapter;
    private TextView tvListaVacia;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_alumnos);
        setTitle(getString(R.string.seleccionar_alumno));

        this.lvAlumnos = findViewById(R.id.lv_alumnos);
        this.tvListaVacia = findViewById(R.id.tv_lista_vacia);
        this.lvAlumnos.setEmptyView(this.tvListaVacia);

//        try {
        BoletinesDataProvider.fetchDNIsYNombresDeAlumnos(new ListaDeAlumnosConsumer() {
            @Override
            public void accept(List<AlumnoData> data) {
                ListaDeAlumnosActivity.this.adapter = new ListaDeAlumnosAdapter(ListaDeAlumnosActivity.this, data);
                ListaDeAlumnosActivity.this.lvAlumnos.setAdapter(ListaDeAlumnosActivity.this.adapter);
                ListaDeAlumnosActivity.this.hideProgressBar();
            }

            @Override
            public void handleError(int stringId, VolleyError error, String url) {
                Log.e(getString(R.string.app_name), getString(stringId), error);
                Log.d(getString(R.string.app_name), "url: " + url);
                ListaDeAlumnosActivity.this.setEmptyMsg(getString(stringId) + "\n\n" + error.getMessage());
                ListaDeAlumnosActivity.this.hideProgressBar();
            }

            @Override
            public void handleException(int stringId, JSONException exception) {
                Log.e(getString(R.string.app_name), getString(stringId), exception);
                ListaDeAlumnosActivity.this.setEmptyMsg(getString(stringId));
                ListaDeAlumnosActivity.this.hideProgressBar();
            }
        });

        this.lvAlumnos.setOnItemClickListener((parent, view, position, id) -> {
            AlumnoData alumnoSeleccionado = ListaDeAlumnosActivity.this.adapter.getItem(position);

            if (alumnoSeleccionado != null) {
                Intent intent = new Intent(ListaDeAlumnosActivity.this, BoletinActivity.class);
                intent.putExtra("dni", alumnoSeleccionado.getDni());
                intent.putExtra("nombre_y_apellido", alumnoSeleccionado.getNombreYApellido());
                intent.putExtra("curso", alumnoSeleccionado.getCurso());
                startActivity(intent);
            }
        });

//        } catch (RuntimeException e) {
//            Toast.makeText(ListaDeAlumnosActivity.this, "BOOM", Toast.LENGTH_SHORT).show();
//        }
    }

    public void hideProgressBar() {
        findViewById(R.id.pb_cargando).setVisibility(View.GONE);
    }

    public void setEmptyMsg(String msg) {
        this.tvListaVacia.setText(msg);
    }

}