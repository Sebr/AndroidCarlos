package ar.edu.unq.ciu.boletines.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ar.edu.unq.ciu.boletines.tools.Opciones;
import ar.edu.unq.ciu.boletines.R;

/**
 * Created by muerte on 12/12/17.
 */

public class OpcionesActivity extends AppCompatActivity {

    private EditText etIP;
    private EditText etPort;
    private EditText etBase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opciones);
        setTitle(getString(R.string.opciones));

        this.etIP = findViewById(R.id.et_ip);
        this.etIP.setText(Opciones.getIp());

        this.etPort = findViewById(R.id.et_port);
        this.etPort.setText(Opciones.getPort());

        this.etBase = findViewById(R.id.et_base);
        this.etBase.setText(Opciones.getBase());

        Button btnLocal = findViewById(R.id.btn_local);
        btnLocal.setOnClickListener(view -> {
            etIP.setText(getString(R.string.local_ip_default));
            etPort.setText(getString(R.string.local_port_default));
            etBase.setText(getString(R.string.local_base_default));
            Toast.makeText(this, "settear ip correcto del servidor", Toast.LENGTH_SHORT).show();
        });

        Button btnHeroku = findViewById(R.id.btn_heroku);
        btnHeroku.setOnClickListener(view -> {
            etIP.setText(getString(R.string.heroku_ip_default));
            etPort.setText(getString(R.string.heroku_port_default));
            etBase.setText(getString(R.string.heroku_base_default));
        });

        Button btnOK = findViewById(R.id.btn_ok);
        btnOK.setOnClickListener(view -> {
            Opciones.setIp(OpcionesActivity.this.etIP.getText().toString());
            Opciones.setPort(OpcionesActivity.this.etPort.getText().toString());
            Opciones.setBase(OpcionesActivity.this.etBase.getText().toString());
            OpcionesActivity.this.finish();
        });
    }

}
