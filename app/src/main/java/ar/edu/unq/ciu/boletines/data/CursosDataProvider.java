package ar.edu.unq.ciu.boletines.data;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;

import ar.edu.unq.ciu.boletines.R;
import ar.edu.unq.ciu.boletines.tools.ApplicationToolset;
import ar.edu.unq.ciu.boletines.tools.Opciones;




public class CursosDataProvider {

    static public void fetchCursos(ListaDeCursosConsumer consumer) {
        String url = Opciones.getUrl() + "/cursos";

        JsonArrayRequest jsar = new JsonArrayRequest(Request.Method.GET, url, null,
                response -> consumer.accept(CursoDataAnalizer.buildListaDeCursos(consumer, response)),
                error -> {
                    consumer.handleError(R.string.error_req_rest, error, url);
//                error.printStackTrace();
//                throw new RuntimeException("error en el request REST", error);
                }
        );

        ApplicationToolset.toolset().addToRequestQueue(jsar);
    }
}