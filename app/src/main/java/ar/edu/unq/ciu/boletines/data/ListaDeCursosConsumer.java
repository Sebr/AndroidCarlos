package ar.edu.unq.ciu.boletines.data;

import com.android.volley.VolleyError;

import org.json.JSONException;

import java.util.List;


public interface ListaDeCursosConsumer {


    void accept(List<CursoData> data);

    void handleError(int stringId, VolleyError error, String url);

    void handleException(int error_parsing_json, JSONException e);

}
