package ar.edu.unq.ciu.boletines.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ar.edu.unq.ciu.boletines.R;
import ar.edu.unq.ciu.boletines.data.CursoData;


public class ListaCursosAdapter extends ArrayAdapter {
    private LayoutInflater inflater;
    public ListaCursosAdapter(@NonNull Context context, @NonNull List objects) {
        super(context, 0, objects);
        this.inflater = LayoutInflater.from(context);
    }
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = this.inflater.inflate(R.layout.cursos_adapter, parent, false);
        }

        CursoData curso = (CursoData) this.getItem(position);

        if (curso != null) {
            TextView tvAnio = convertView.findViewById(R.id.tv_anio);
            tvAnio.setText(curso.getAnio());

            TextView tvDiv = convertView.findViewById(R.id.tv_division);
            tvDiv.setText(curso.getDivision());

            TextView tvTurno = convertView.findViewById(R.id.tv_turno);
            tvTurno.setText(curso.getTurno());

        }

        return convertView;
    }
        }

