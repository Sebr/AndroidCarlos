package ar.edu.unq.ciu.boletines.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ar.edu.unq.ciu.boletines.R;

/**
 * Created by muerte on 12/12/17.
 */

class AlumnoDataAnalizer {

    static List<AlumnoData> buildListaDeAlumnos(ListaDeAlumnosConsumer consumer, JSONArray jsonData) {
        List<AlumnoData> res = new ArrayList<>();
        try {
            for (int i = 0; i < jsonData.length(); i++) {
                JSONObject jsa = jsonData.getJSONObject(i);
                res.add(AlumnoDataAnalizer.decodeAlumno(consumer, jsa));
            }
        } catch (JSONException e) {
            consumer.handleException(R.string.error_parsing_json, e);
//            throw new RuntimeException(activity.getString(R.string.error_parsing_json), e);
        }
        return res;
    }

    static private AlumnoData decodeAlumno(ListaDeAlumnosConsumer consumer, JSONObject jsa) {
        AlumnoData ad = null;
        try {
            ad = new AlumnoData(jsa.getString("dni"), jsa.getString("nombreYApellido"));
            try {
                ad.setCurso(jsa.getString("curso"));
                ad.setMail(jsa.getString("mail"));
            } catch (JSONException e) {
                // el alumno puede no estar inscripto en un curso => no pasa nada
            }
        } catch (JSONException e) {
            consumer.handleException(R.string.error_parsing_json, e);
//            throw new RuntimeException(activity.getString(R.string.error_parsing_json), e);
        }
        return ad;
    }

}
