package ar.edu.unq.ciu.boletines.data;

import com.android.volley.VolleyError;

import org.json.JSONException;

import java.util.List;

/**
 * Created by muerte on 12/12/17.
 */

public interface ListaDeAlumnosConsumer {

    void accept(List<AlumnoData> data);

    void handleError(int stringId, VolleyError error, String url);

    void handleException(int error_parsing_json, JSONException e);
}
