package ar.edu.unq.ciu.boletines.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.android.volley.VolleyError;

import org.json.JSONException;

import java.util.List;

import ar.edu.unq.ciu.boletines.R;
import ar.edu.unq.ciu.boletines.data.AlumnoData;
import ar.edu.unq.ciu.boletines.data.BoletinesDataProvider;
import ar.edu.unq.ciu.boletines.data.ListaDeAlumnosConsumer;

public class ListaDeAlumnosCris extends AppCompatActivity {
private ListView listView;
private ListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_de_alumnos_cris);
        listView= findViewById(R.id.listview);

        BoletinesDataProvider.fetchDNIsYNombresDeAlumnos(new ListaDeAlumnosConsumer() {
            @Override
            public void accept(List<AlumnoData> data) {
                ListaDeAlumnosCris.this.adapter = new ListaAlumnosCrisAdapter( ListaDeAlumnosCris.this, data);
                ListaDeAlumnosCris.this.listView.setAdapter( ListaDeAlumnosCris.this.adapter);
//                ListaDeAlumnosCris.this.hideProgressBar();
            }

            @Override
            public void handleError(int stringId, VolleyError error, String url) {
                Log.e(getString(R.string.app_name), getString(stringId), error);
                Log.d(getString(R.string.app_name), "url: " + url);
//                ListaDeAlumnosCris.this.setEmptyMsg(getString(stringId) + "\n\n" + error.getMessage());
//                ListaDeAlumnosCris.this.hideProgressBar();
            }

            @Override
            public void handleException(int stringId, JSONException exception) {
                Log.e(getString(R.string.app_name), getString(stringId), exception);
//                ListaDeAlumnosCris.this.setEmptyMsg(getString(stringId));
//                ListaDeAlumnosCris.this.hideProgressBar();
            }
        });
        this.listView.setOnItemClickListener((parent, view, position, id) -> {
            AlumnoData alumnoSeleccionado = (AlumnoData) ListaDeAlumnosCris.this.adapter.getItem(position);

            if (alumnoSeleccionado != null) {
                Intent intent = new Intent(ListaDeAlumnosCris.this, BoletinActivity.class);
                intent.putExtra("dni", alumnoSeleccionado.getDni());
                intent.putExtra("nombre_y_apellido", alumnoSeleccionado.getNombreYApellido());
                intent.putExtra("curso", alumnoSeleccionado.getCurso());
                startActivity(intent);
            }
        });

//        } catch (RuntimeException e) {
//            Toast.makeText(ListaDeAlumnosActivity.this, "BOOM", Toast.LENGTH_SHORT).show();
//        }
    }

    }


