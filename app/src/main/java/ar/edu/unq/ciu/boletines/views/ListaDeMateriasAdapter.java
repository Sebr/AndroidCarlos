package ar.edu.unq.ciu.boletines.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ar.edu.unq.ciu.boletines.R;
import ar.edu.unq.ciu.boletines.data.MateriaData;

/**
 * Created by muerte on 12/12/17.
 */

class ListaDeMateriasAdapter extends ArrayAdapter<MateriaData> {

    private final LayoutInflater inflater;

    ListaDeMateriasAdapter(Context context, List<MateriaData> data) {
        super(context, 0, data);
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = this.inflater.inflate(R.layout.materia_list_item, parent, false);
        }

        MateriaData materia = getItem(position);
        if (materia != null) {
            TextView tvMateria = convertView.findViewById(R.id.tv_materia);
            tvMateria.setText(materia.getNombre());

            this.mostrarDatoSiExiste(convertView.findViewById(R.id.tv_nota1), materia.getNota1());
            this.mostrarDatoSiExiste(convertView.findViewById(R.id.tv_nota2), materia.getNota2());
            this.mostrarDatoSiExiste(convertView.findViewById(R.id.tv_nota3), materia.getNota3());
            this.mostrarDatoSiExiste(convertView.findViewById(R.id.tv_condicion), materia.getCondicion());
        }

        return convertView;
    }

    private void mostrarDatoSiExiste(TextView tv, String dato) {
        if (dato != null) {
            tv.setText(dato);
        }
    }

}
